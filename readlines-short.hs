readLines :: Int -> IO [String]
readLines n = do 
	result <- readLineIter n (return []) 
	return $ reverse result where 
	readLineIter n acc = if n == 0 then acc else do
		lines <- (readLineIter (n - 1) acc) 
		newLine <- getLine
		return ( newLine:lines )
main = do 
	lines <- readLines 3
	putStr $ show lines