{-# LANGUAGE ScopedTypeVariables #-}


data Point num = Point {x::num, y::num} deriving (Show)
type PointChange num = Point num -> Point num -> Point num
data Turtle num a =     Turtle { direction,position ::Point num,       cargo::a } 
                   |    Command{ dirChange,posChange::PointChange num, cargo::a } 

instance (Num num) => Monad (Turtle num) where
    return x = Turtle (Point 0 1) (Point 0 0) x
    (Turtle d p x) >>= func = apply (func x) where 
        apply t@(Turtle _ _ _)  = t
        apply (Command cd cp cc) = Turtle (cd d p) (cp d p) cc
    (Command cd1 cp1 x) >>= func = compose (func x) where
        compose t@(Turtle _ _ _) = t
        compose (Command cd2 cp2 y) = 
            let apply change d p = change (cd1 d p) (cp1 d p )
            in Command (apply cd2 ) (apply cp2) y
 
keepDir d p = d
keepPos d p = p

turnMatrix::Num num => ((num,num),(num,num)) -> a -> Turtle num a
turnMatrix ((txx, txy), (tyx, tyy)) z = 
    let dirChange (Point dx dy) p = Point (dx * txx + dy * txy) (dx * tyx + dy * tyy) 
    in Command dirChange keepPos z 

applyDir::Num num => (num -> num -> num) -> num -> a -> Turtle num a
applyDir op q z =   let apply (Point dx dy) (Point px py) = Point (op px (q * dx))(op py (q * dy))
                    in Command keepDir apply z 

turnLeft  = turnMatrix ((0,-1),(1,0))
turnRight = turnMatrix ((0,1),(-1,0))
turnBack  = turnMatrix ((-1,0),(0,-1))
forward   = applyDir (+)
back      = applyDir (-) 

main = do 
    let turtle = return 5 :: Turtle Int Int
        move turt = do
            t <- turt
            forward 5 t
            turnRight t
            forward 10 t
            turnLeft t 
            back 3 t
    print $ position $ move turtle





