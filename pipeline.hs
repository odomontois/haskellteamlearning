(|>) = flip ($)
(<|) = ($)

main =  do
        let function1 x = x + 1
            function2 x = x * 2
            result = function2 <| function1 100
        print result