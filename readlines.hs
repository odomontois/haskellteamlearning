readLines :: Int -> IO [String]
readLines n = (readLineIter n (return []) )>>= makeResult where 
	makeResult :: [String] -> IO [String]
	makeResult lines = return ( reverse lines )
	readLineIter :: Int -> IO [String] -> IO [String]
	readLineIter n acc = if n == 0 then acc else (readLineIter (n - 1) acc) >>= appendNewLine where 
		appendNewLine :: [String] -> IO [String]
		appendNewLine lines = getLine >>= ( appendLine lines ) where
			appendLine::[String] -> String -> IO [String]
			appendLine lines newLine = return ( newLine:lines )
main = (readLines 3 ) >>= writeLines where 
	writeLines::[String] -> IO ()
	writeLines lines = putStr ( show lines )
