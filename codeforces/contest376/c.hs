import Data.List
import Data.Char
import Data.Maybe
chars = "1689"
modulus = 7
base = 10
notFound = "0"
clearChars xs = (foldr1 (.) (map delete chars) ) xs
mods xs = foldl' (\p d-> mod (base * p + digitToInt d) modulus) 0 xs
findPrefix str = find suitable (permutations chars) where
    suitable prefix = (mod (mods prefix * modPower + modStr) modulus) == 0
    modPower        = mods ('1':replicate (length str) '0')
    modStr          = mods str
solution str = do 
    let cleared = clearChars str
    prefix  <- findPrefix cleared
    return (prefix ++ cleared) 

main = do
    str <- getLine
    putStrLn ( fromMaybe notFound ( solution str ) )
