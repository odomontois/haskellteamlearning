import Data.List
import Data.Map (fromListWith, toList)
data Debt = Debt { debtor ::Int,  creditor ::Int,  amount ::Int } deriving Show
readInts = do 
	line <- getLine
	return ( map read (words line) :: [Int] )
readDebt = do
	ints <- readInts
	case ints of [debtor, creditor, amount] -> return ( Debt debtor creditor amount)
readDebts k = ( sequence $ replicate k readDebt ) >>= dualize
	where dualize xs = return $ foldl' ( \acc x-> dualDebt x ++ acc ) [] xs

dualDebt (Debt debtor creditor amount) = [Debt debtor creditor amount, Debt creditor debtor (-amount)] 
sumDebts debts = fromListWith (+) ( map (\d -> (debtor d, amount d)) debts )
result debts = div ( foldl' (\ z (k,d)-> ( (abs d ) + z )) 0 ( toList $ sumDebts  debts ) ) 2

main = do 
	[n,m] <- readInts
	debts <- readDebts m

	print $ result debts

