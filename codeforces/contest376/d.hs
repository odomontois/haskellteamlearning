import Data.List
import Data.Map (fromListWith,toList)
streaks xs = reverse $ snd $ foldl' (\(c,r) x -> if x == '1' then (c+1, c+1:r) else (0,0:r)) (0,[]) xs
streakCount xss =  map ( toList . fromListWith (+) . flip zip (repeat 1)) (transpose (map streaks xss))
columnStreaks streakCnt = reverse $ snd $ foldl' (\(a,r) (l,c) -> (a+c,(l,a+c):r) ) (0,[]) (reverse $ sort streakCnt)
solution xss = maximum $ map ( maximum . map (\(l,c) -> l*c) . columnStreaks ) $ streakCount xss

main = do
    line <- getLine
    matrix <- sequence $ replicate (n line) getLine
    print $ solution matrix
    where n line = read (head $ words line ) :: Int