import Data.List
splitBy :: Eq a => a -> [a] -> [[a]]
splitBy sep xs = unfoldr split xs
	where 
	split [] = Nothing 
	split xs = case elemIndex sep xs of
		Nothing -> Just(xs,[])
		Just i -> Just (segment,rest)
			where 
				pair = splitAt i xs
				segment = fst pair
				rest = drop 1 (snd pair)
readInts = map read ::Int (splitBy ' ' line) 

