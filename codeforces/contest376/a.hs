import Data.Int (Int64)
import Data.List (foldl')
import Data.Char (digitToInt)
main = do 
	x <- getLine
	putStrLn ( find x )
	where
 	parseChar c = if c == '=' then 0 else fromIntegral( digitToInt c )
	force xs = foldl' calc  0 (zip [1..] (map parseChar xs ) )
		where 
			calc :: Int64 -> (Int64,Int64) -> Int64
			calc acc (len,weight) = (weight * len + acc)
	find s | left  == right = "balance"
           | left  >  right = "left"
           | left  <  right = "right"
		where 
			notSep c = c /= '^'
			left  = force ( reverse( takeWhile notSep s ) )
			right = force ( drop 1 ( dropWhile notSep s ) )