import Control.Applicative
main = do
    n <- read <$> getLine ::IO Int
    cards <- map read <$> words <$> getLine ::IO [Int]
    putStrLn $ answer n cards where
        answer n cards = unwords $ map show $ result n cards
        split acc1 acc2 [] = [acc2,acc1]
        split acc1 acc2 (x:xs) = split acc2 (x:acc1) xs
        result n cards = map sum $ split [] [] $ find n cards (reverse cards) []
        find 0 _ _ acc = acc
        find n (a:as) (b:bs) acc | a > b     = find (n-1) as (b:bs) (a:acc)
                                 | otherwise = find (n-1) (a:as) bs  (b:acc) 
