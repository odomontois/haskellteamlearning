import Data.List
main = do
    n <- read `fmap` getLine ::IO Int
    as <- map read `fmap` words `fmap` getLine::IO [Int]
    let (u,v) = solution as in do
        print $ length u + length v
        putStrLn $ (write $ reverse u) ++ " " ++ write v 
    where
        write = unwords . map show
        solution as = case snd $ foldl' walk ((-1, False),([], [])) $ sort as of
            ((u:us),(v:vs)) | u == v -> ((u:us),vs)
            t                        -> t
        walk ((prev,rep),(us,vs)) a | prev /= a = ((a,False),(a:us,vs))
                                    | rep       = ((a,True) ,(us,vs))
                                    | otherwise = ((a,True) ,(us,a:vs))