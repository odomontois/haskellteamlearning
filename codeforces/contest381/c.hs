import Data.Int (Int64)
import Data.List
import Control.Applicative
import GHC.Exts
data Command = Element Int | Copy Int Int deriving (Show,Eq,Ord)
data Request = Request{reqIndex::Int, reqValue::Int64} deriving (Show,Eq,Ord)
data Result = Result Int Int deriving(Show, Eq, Ord)
commands = map command where 
    command [1,x]   = Element x
    command [2,x,y] = Copy x y

comlen :: Command -> Int64
comlen (Element x) = 1
comlen (Copy x y) = fromIntegral x * fromIntegral y
walk::[Command] -> [Request] -> [Result]
walk = iter [] where
    iter ress _ [] = ress
    iter ress comms reqs = let 
        (ress',reqs') = check ress comms reqs 0 []
        in iter ress' comms reqs'
    check ress _ [] _ reqs' = (ress,reqs')
    check ress (Element x:comms) (Request i v:reqs) prev reqs' | prev + 1 == v = check (Result i x:ress) comms reqs (prev+1) reqs'
    check ress (comm@(Copy x y):comms)(Request i v:reqs) prev reqs'| prev + comlen comm >= v = 
        check ress comms reqs (prev + comlen comm) (Request  i ((v-prev) `mod` fromIntegral x) : reqs')
    check ress (comm:comms) reqs prev reqs' = check ress comms reqs (prev + comlen comm) reqs'
    check ress [] reqs prev reqs' = error("Empty command list " ++ show ress ++ show reqs ++ show prev ++ show reqs' )


main = do
    n <- read <$> getLine :: IO Int
    comms <-  commands <$> map (map read) <$> map words <$> (sequence $ replicate n getLine )
    m <- read <$> getLine ::IO Int
    reqs <- sort <$> map (uncurry Request) <$> zip [0..] <$> map read <$> words <$> getLine
    print comms
    print reqs 
    print $ walk comms $ reqs