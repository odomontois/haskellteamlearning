import Data.Maybe 
import Data.List
steps:: Int -> Int -> Int -> Maybe Int
steps x y l = if x > y then steps y x l else if ( mod (y-x) l ) == 0 then Just $ div (y-x) l else Nothing
stepEdge x m l = catMaybes [ steps 1 x l, steps m x l ]
possMoves n m x y a b = mod ( y -x ) 2 == 0 && ( x == y || (y > x && m >= b ) || (x > y && n >= a)  )
solution n m i j a b = catMaybes [ if possMoves n m x y a b then Just $ maximum [x,y] else Nothing | x <- stepEdge i n a , y <- stepEdge j m b ]
main = do
	string <- getLine  
	print (( if null $ sol string then "Poor Inna and pony!" else show $ minimum $ sol string )  ++ debug string )
	where 
		sol string =  solution n m i j a b
			where [n,m,i,j,a,b] = map read $ words string
		debug string = show [ stepEdge i n a , stepEdge j m b]
			where [n,m,i,j,a,b] = map read $ words string


