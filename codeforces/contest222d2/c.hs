import Control.Applicative
import Data.List as L
import Data.Tuple
import Data.Maybe
import qualified Data.Map as M
import qualified Data.Set as S

data Cell = Cell {row::Int,col::Int} deriving (Show,Eq,Ord)

main = do
    [n,m,k] <- map read <$> words <$> getLine :: IO[Int] 
    lab <- M.fromList <$> foldl1' (++) <$> 
          map (\(row,list) -> map (\(col,e) -> (Cell row col,e)) $ zip [0..] list  ) <$> 
          zip [0..] <$> lines <$> getContents 
    let solution n m k lab = let              
            walk::[(Int,Cell)] -> S.Set Cell -> S.Set Cell -> [Cell]-> Int -> [(Int,Cell)]
            walk levels prev cur strip level = let
                visited     = S.union prev cur
                nextSet     = S.fromList neighbours
                next        = S.toList nextSet
                neighbours  = do 
                    original <- strip
                    t        <- [(-1,0),(1,0)]
                    (i,j)    <- [t,swap t]
                    let x       = row original + i 
                        y       = col original + j
                        cell    = Cell x y
                    if  x >= 0              && 
                        x < n               && 
                        y >= 0              && 
                        y < m               &&  
                        lab M.! cell == '.' && 
                        S.notMember cell visited
                        then return cell 
                        else [] 
                in if null next 
                    then levels 
                    else walk (zip (repeat level) next ++ levels) cur nextSet next (level + 1)
            start = fst $ fromJust $ find ((== '.') . snd) $ M.assocs lab
            walkList = walk [(0,start)] S.empty (S.singleton start) [start] 1 
            wall = S.fromList $ map snd $ take k $ reverse $ sort walkList 
            result = [[ if lab M.! cell == '#' 
                    then '#' 
                    else if S.member cell wall 
                        then 'X' 
                        else '.' 
                | j <- [0..m-1], let cell = Cell i j] | i <- [0..n-1] ]
            countX = length $ filter (== 'X' ) $ foldl1' (++) result
            in result{- ++[show countX]-}
    mapM putStrLn $ solution n m k lab