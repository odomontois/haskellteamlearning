variants a b d res@[win1,tie,win2] = if d == 7 then res else  variants a b (d+1) ( 
	if x1 < x2 then [win1 + 1, tie , win2] else if x1 > x2 then [win1, tie, win2+1] else [win1, tie+1,win2])
	where 
		x1 = abs(a - d)
		x2 = abs(b - d)
main = do
	string <- getLine
	putStrLn $ result string 
	where result string =  show win1 ++ " " ++ show tie ++ " "++ show win2
		where 
			[a,b] = map read $ ( words string )
			[win1,tie,win2] = variants a b 1 [0,0,0]