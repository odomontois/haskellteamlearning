import Data.List
import Data.Ord
import Control.Applicative

main = do
    n <- read <$> getLine :: IO Int
    participants <- transpose <$> (map $ map read . words) <$> lines <$> getContents :: IO [[Int]]
    let 
        mul = 1048576
        indexed = [zip (participants!!i) [ i*mul..]  | i<-[0,1]]
        winners = markWinners 0 [] where
            markWinners place acc quotes ((res,num):xs) | place < quotes  = markWinners (place+1) ((num,True):acc) quotes  xs
                                                        | otherwise       = markLosers  acc ((res,num):xs)
            markWinners place acc quotes []= acc 
            markLosers  acc ((res,num):xs) = markLosers ((num,False):acc) xs
            markLosers  acc []             = acc
        local =   (map snd ) <$> sort <$> winners (n `div` 2)  <$> sort <$> indexed
        global = map (map snd) $ groupBy (\ a b -> fst  a `div` mul == fst b `div` mul ) $
            sort $ winners n $ sort $ foldl1' (++) indexed 
        --debug =[show indexed, show global, show local, show $ sort$  markWinners n 0 [] $ sort $ foldl1' (++) indexed]
    mapM putStrLn $ (map ( map (\(x,y) -> if x || y then '1' else '0'))  $ map (\ (a,b) -> zip a b ) $ zip local global )