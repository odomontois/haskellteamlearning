import Data.List
import Control.Applicative
import Data.Maybe
one bool = if bool then 1 else 0
data Search = Search{count::Integer,startC::Bool,endA::Bool} deriving (Eq,Show,Ord)
(+!) :: Search -> Search -> Search
a +! b = Search (count a + count b + (one $ endA a && startC b )) (startC a) (endA b)
specChars:: Search -> Int
specChars s = (one $ startC s) + (one $ endA s)
main = do
	[ k, x, n, m ] <- (map read) <$> words <$> getLine ::IO [Int]
	mapM putStrLn $ solution k x n m where 
		solution k x n m = let	
			iter a b k = if k == 2 then b else iter b (a+!b) (k - 1 )
			iterS a b k = if k == 2 then b else iterS b (a++b) (k - 1 )
			choices n = if n == 0 then [[]] else [c:cs| c<-[False,True], cs <- choices (n-1)] 
			variants = [(Search (toInteger a) xa ya ,Search (toInteger b) xb yb) | [xa,ya, xb, yb] <-(choices 4), 
				a <- [0..div (n - one xa - one ya ) 2], 
				b <- [0..div (m - one xb - one yb ) 2]]
			search = find fitting variants where
				fitting (a,b) = (count $ iter a b k) == (toInteger x)
			complete u l = 	(if startC u then "C" else []) ++ 
						   	replicate (l - (2 * (fromIntegral $ count u)) - specChars u) 'X' ++ 
						   	(foldl' (++) "" $ replicate (fromIntegral $ count u) "AC") ++
						   	if endA u then "A" else []
			strings = do
				(a,b) <- search
				return [complete a n, complete b m]
			in fromMaybe  ["Happy new year!"] strings
			--in [show $ choices k]