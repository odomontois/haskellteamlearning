import Control.Applicative
import Data.List

main = do
	n <- read <$> getLine ::IO Int
	a <- (map read) <$> words <$> getLine:: IO [Int]
	putStrLn $ foldl1' (++) $ map (line n a) [1..maximum a] where
		line n a i = (foldl1' (++) $ map (\(x,k) -> coin x ++ move k ) $ zip a [1..] ) ++ replicate (n - 1) 'L' where
			coin x = if x >= i then "P" else []
			move k = if k < n then "R" else []