import Control.Applicative
candles a b r = if a == 0 then 0 else a + candles (div (a + r) b) b (mod (a + r) b)
main = do
	[a,b] <- map read <$> words <$> getLine ::IO[Int]
	print $ candles a b 0