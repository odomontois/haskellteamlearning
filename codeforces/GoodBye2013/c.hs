import Data.List
import Control.Applicative
import Data.Ord
main = do
	n <- read <$> getLine :: IO Int
	a <- map read <$> words <$> getLine :: IO [Int]
	putStrLn $ intercalate " " $  solution a where
		solution a = let 
			b = snd $ foldl' assign (0,[]) $ sort $ zip a [0..]
			assign (prev,acc) (val,idx) = if val <= prev then (prev +1 , (prev+1,idx):acc) else (val, (val,idx):acc)
			in map (show . fst) $ sortBy (comparing snd) b